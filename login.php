
<!DOCTYPE html>
<html>
<head>
	<title>Please Log in </title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">

</head>
<body>
<br>
<div class="row">
	 <div class="col-sm-4"></div>
			<div class="col-sm-4"><h3>
			<?php
			session_start();
			if(isset($_SESSION['msg']))
			{
				echo $_SESSION['msg'];
				session_unset($_SESSION['msg']);
			}

			?></h3>
			</div>
	 <div class="col-sm-4"></div>
</div>

 <div class=" row">
<div class="col-sm-4"></div>
 <div class="col-sm-4">
      <form class="form-signin" method="post" action="login_process.php">
        <h2 class="form-signin-heading">Please sign in</h2>
        <label class="sr-only">UserName:</label>
        <input type="text" class="form-control" placeholder="Username"  name="username" required autofocus>
        <label class="sr-only">Password</label>
        <input type="password" class="form-control" placeholder="Password" name="password" required>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
      </form>
</div>
<div class="col-sm-4"></div>
</div> 
</body>
</html>